# PowerFactoryScripts
This repository contains a library of more than 100 DPL ([DIgSILENT PowerFactory](https://www.digsilent.de/en/powerfactory.html)) scripts developed by [Energinet](https://energinet.dk) to be used to deal with [CGMES](https://docstore.entsoe.eu/major-projects/common-information-model-cim/cim-for-grid-models-exchange/standards/Pages/default.aspx)  and "dynamify" a steady state model .

You are encouraged to test and suggest improvements in order to develop a common script library which can be used for the [ENTSO-e](https://www.entsoe.eu) SPD activities. 
